import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { Http } from '@angular/http';

import { DetailPage } from '../detail/detail';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/timeout';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

  posts: Array<Object>;

  constructor(public navCtrl: NavController, public http: Http) {
      this.getData();
  }

  getData() {
      this.posts = [];
      /* 100 posts */
      this.http.get('https://www.dordtsport.nl.blis.ws/wp-json/wp/v2/posts?per_page=100&_embed').map(res => res.json()).subscribe(data => {
        for(let i = 0; i < data.length; i++) {
            let image = "";
            if(data[i]._embedded["wp:featuredmedia"].length > 0) {
                if(data[i]._embedded["wp:featuredmedia"][0].media_details) {
                    console.log(data[i]._embedded["wp:featuredmedia"][0].media_details);
                    if(data[i]._embedded["wp:featuredmedia"][0].media_details.sizes["post-thumbnail-alt"]) {
                        image = data[i]._embedded["wp:featuredmedia"][0].media_details.sizes["post-thumbnail-alt"].source_url;
                    } else if(data[i]._embedded["wp:featuredmedia"][0].media_details.sizes["full"]) {
                        image = data[i]._embedded["wp:featuredmedia"][0].media_details.sizes["full"].source_url;
                    }
                }
            }

            console.log('image = '+image);

            this.posts.push({
                "id": data[i].id,
                "title": data[i].title.rendered,
                "content": data[i].content.rendered,
                "featured_media": data[i].featured_media,
                "embedded": data[i]._embedded,
                "image": image
            });
        }
      });
  }

  show(post) {
      this.navCtrl.push(DetailPage, {post: post});
  }

}
