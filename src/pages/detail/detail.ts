import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/timeout';


@Component({
    selector: 'page-home',
    templateUrl: 'detail.html'
})
export class DetailPage {

    post: Array<any>;
    imageURL: string;

    constructor(public navCtrl: NavController, public http: Http, public navParams: NavParams) {
        this.post = this.navParams.get("post");
        console.log(this.post);
    }



}
